FROM nginx:stable-alpine

COPY nginx.conf /etc/nginx/conf.d/nginx.conf
COPY ./index.html /var/www/html/index.html
